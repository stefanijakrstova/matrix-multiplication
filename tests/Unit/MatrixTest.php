<?php

namespace Tests\Unit;

use App\Classes\Matrix;
use Tests\TestCase;


class MatrixTest extends TestCase
{

    public function testCreateMatrixFromArray(): void
    {
        $array = [[1, 2, 3, 4], [4, 3, 2, 1], [1, 2, 3, 4]];
        $matrix = new Matrix($array);
        self::assertEquals(3, $matrix->getRows());
        self::assertEquals(4, $matrix->getColumns());
    }

    public function testThrowExceptionOnMultiplyIncompatibleMatrices(): void
    {
        $this->expectException(\Exception::class);
        $matrix1 = new Matrix([[1, 2, 3, 4], [4, 3, 2, 1]]);
        $matrix2 = new Matrix([[1, 2, 3, 4], [4, 3, 2, 1]]);
        $matrix1->multiply($matrix2);
    }

    public function testMatrixMultiply(): void
    {
        $matrix1 = new Matrix([
            [1, 2, 3, 4],
            [4, 3, 2, 1],
        ]);
        $matrix2 = new Matrix([
            [5, 6],
            [6, 5],
            [7, 8],
            [8, 7]
        ]);
        $product = [
            [70, 68],
            [60, 62],
        ];
        self::assertEquals($product, $matrix1->multiply($matrix2, false)->matrix);
    }

    public function testMatrixFormatToExcelColumns(): void
    {
        $matrix1 = new Matrix([
            [1, 2, 3, 4],
            [4, 3, 2, 1],
        ]);
        $matrix2 = new Matrix([
            [5, 6],
            [6, 5],
            [7, 8],
            [8, 7]
        ]);
        $product = [
            ["BR", "BP"],
            ["BH", "BJ"],
        ];
        self::assertEquals($product, $matrix1->multiply($matrix2)->matrix);
    }
}
