## Project title

Matrix multiplication

## Tech/framework used
- <b>Built with</b>
   -Laravel
- <b>Database</b>
   -MySQL
- <b>Authentication</b>
   -Passport (personal token)
- <b>Documentation</b>
   -Swagger

## Features

Multiplication of two matrices with the necessary validation

## Tests

Unit tests for testing successful matrix creation and multiplication

## How to use?

The user needs first to register/login in order to obtain a personal access token, and after the multiply-matrix endpoint can be called with the given token for authentication.

