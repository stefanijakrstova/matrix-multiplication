<?php

namespace App\Http\Controllers;

use App\Classes\Matrix;
use App\Http\Requests\Matrix\CreateRequest;

class MatrixController extends Controller
{
    /**
     * @OA\Info(
     *     title="Matrix manipulation API",
     *     version="1.0"
     * )
     */

    /**
     * @OA\Post(
     * 	    path="/matrix-multiplication",
     * 	    operationId="multiplyTwoMatrices",
     * 	    summary="Multiplication of two matrices",
     *      description="Multiplication of two matrices that resuls in excel-column matrix data",
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *              required={"matrix1","matrix2"},
     *                  @OA\Property(
     * 		                property="matrix1",
     *                      description="The matrix should consist of arrays for each row",
     *                      @OA\Schema(
     *                          type="array",
     *                          @OA\Items(
     *                              type="integer"
     *                          ),
     *                      ),
     *                  ),
     *                  @OA\Property(
     * 		                property="matrix2",
     *                      description="The matrix should consist of arrays for each row",
     *                      @OA\Schema(
     *                          type="array",
     *                          @OA\Items(
     *                              type="integer"
     *                          ),
     *                      ),
     *                  ),
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="success",
     * 	    ),
     * )
     */
    public function matrixMultiplication(CreateRequest $request)
    {
        //Create matrices from arrays
        $matrix1 = new Matrix($request->matrix1);
        $matrix2 = new Matrix($request->matrix2);

        //Multiply matrices
        $multipliedMatrix = $matrix1->multiply($matrix2,false);

        //Get the resulting matrix as array
        $resultingMatrix = $multipliedMatrix->matrix;
        return response()->json(['matrix' => $resultingMatrix], 200);
    }
}
