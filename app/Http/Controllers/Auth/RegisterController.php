<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  App\Http\Requests\Auth\RegisterRequest  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * @OA\Post(
     * 	    path="/register",
     * 	    operationId="register",
     * 	    summary="Register new user",
     *      description="Register new user in system",
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *              required={"name", "email", "password", "password_confirmation"},
     *                  @OA\Property(
     * 		                property="name",
     *                      description="User's name",
     *                      type="string",
     *                  ),
     *                  @OA\Property(
     * 		                property="email",
     *                      description="User's email",
     *                      type="email",
     *                  ),
     *                  @OA\Property(
     * 		                property="password",
     *                      description="User's password",
     *                      type="string",
     *                      format="password"
     *                  ),
     *                  @OA\Property(
     * 		                property="password_confirmation",
     *                      description="User's password confirmation",
     *                      type="string",
     *                      format="password"
     *                  ),
     *              ),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="success",
     * 	    ),
     * )
     */
    public function register(RegisterRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('matrix-access-client')->accessToken;

        return response()->json(['success' => $success], 200);
    }
}
