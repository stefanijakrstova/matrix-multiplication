<?php

namespace App\Http\Requests\Matrix;

use App\Rules\MatrixValidate;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'matrix1' => ['required', 'array', new MatrixValidate($this->matrix2)],
            'matrix2' => ['required', 'array']
        ];
    }
}
