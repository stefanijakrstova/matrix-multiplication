<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class MatrixValidate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $matrix2)
    {
        $this->matrix2 = $matrix2;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $matrix1Columns = count($value[0]);
        $matrix2Rows = count($this->matrix2);

        return ($matrix1Columns === $matrix2Rows);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The matrices are not in the correct format for multiplication.';
    }
}
