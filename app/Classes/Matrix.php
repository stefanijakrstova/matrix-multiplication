<?php
namespace App\Classes;

class Matrix
{
    /**
     * @var array
     */
    public $matrix = [];

    /**
     * @var int
     */
    private $rows;

    /**
     * @var int
     */
    private $columns;

    public function __construct(array $matrix)
    {
        //Set matrix rows
        $this->rows = count($matrix);

        //Set matrix columns
        $this->columns = count($matrix[0]);

        //Check if matrix array is in correct format
        $this->validateMatrix($matrix);

        $this->matrix = $matrix;
    }

    protected function validateMatrix($matrix)
    {
        for ($i = 0; $i < $this->rows; ++$i) {
            if (count($matrix[$i]) !== $this->columns) {
                throw new \Exception('The matrices are not in the right format for multiplication!');
            }
        }
    }

    public function multiply(self $matrix, $transform = true): self
    {
        if ($this->columns !== $matrix->getRows()) {
            throw new \Exception('The matrices are not in the right format for multiplication!');
        }
        $martixArray1 = $this->matrix;
        $martixArray2 = $matrix->matrix;
        $columnsNo = $matrix->columns;

        foreach ($martixArray1 as $row => $rowValues) {
            for ($column = 0; $column < $columnsNo; ++$column) {
                $columnData = array_column($martixArray2, $column);

                $sum = 0;
                foreach ($rowValues as $key => $value) {
                    $sum += $value * $columnData[$key];
                }
                $multipliedMatrix[$row][$column] = $sum;
            }
        }
        if(!$transform)
            return new self($multipliedMatrix);
        //Transform matrix in excel format
        $transformedMatrix = $this->excelFormatMatrix($multipliedMatrix);

        return new self($transformedMatrix);
    }

    private function excelFormatMatrix($matrix) : array
    {
        foreach ($matrix as $row => $matrixRowArray) {
            foreach ($matrixRowArray as $index => $value) {
                $matrix[$row][$index] = $this->excelFormatNumber($value);
            }
        }

        return $matrix;
    }

    private function excelFormatNumber($number) : string
    {
        $numeric = ($number - 1) % 26;
        $letter = chr(65 + $numeric);
        $formatedValue = intval(($number - 1) / 26);
        if ($formatedValue > 0) {
            return $this->excelFormatNumber($formatedValue) . $letter;
        } else {
            return $letter;
        }
    }

    public function getRows(): int
    {
        return $this->rows;
    }

    public function getColumns(): int
    {
        return $this->columns;
    }
}
